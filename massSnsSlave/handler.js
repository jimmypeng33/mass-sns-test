const Bottleneck = require("bottleneck/light");
const aws = require("aws-sdk");
const sns = new aws.SNS();

module.exports.index = async event => {
  const { endpoint, numberOfRequests, requestsPerSecond, slaveId } = event;
  console.log(`Slave ID: ${slaveId}. Making ${numberOfRequests} requests at ${requestsPerSecond} requests/second to endpoint ${endpoint}.`);
  const limiter = new Bottleneck({
    minTime: 1000 / requestsPerSecond
  });
  try {
    const promiseFunctions = [];
    for (let i = 0; i < numberOfRequests; i++) {
      promiseFunctions.push(
        limiter.schedule(() => {
          return publishSns(`test message ${i}`, endpoint);
        })
      );
    }
    await Promise.all(promiseFunctions);
    console.log("Done.");
    return "Done.";
  } catch (err) {
    return `Error. ${err.message}`;
  }
};

const publishSns = async (message, topicArn) => {
  try {
    const params = {
      Message: message,
      TargetArn: topicArn
    };
    console.log(`Publishing message to SNS with payload: ${JSON.stringify(params)}`);
    const result = await sns.publish(params).promise();
    console.log(result);
    console.log(`Message successfully published to topic: ${topicArn}.`);
  } catch (err) {
    console.error(`Unable to publish message ${message}.`);
    throw err;
  }
};
