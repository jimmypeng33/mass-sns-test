const aws = require("aws-sdk");
aws.config.update({
  maxRetries: 0,
  httpOptions: {
    timeout: 300000
  }
});
const lambda = new aws.Lambda();

const testEndpoint =
  "arn:aws:sns:ap-southeast-2:xxxxx:endpoint/GCM/xxxxx/xxxxx";
const totalRequests = 250000;
const totalSlaves = 40;
const slaveRequestPerSecond = 50;
/**
 * One slave has a observed maximum of 150 requests per second.
 * But due to network / lower layer of aws sdk behaviour, it is best to keep this value low.
 * Instead, create more slave instances to reach the total desired requests per second.
 */

module.exports.index = async () => {
  console.log(`Starting with ${totalSlaves} slaves.`);
  const slavePromises = [];
  for (let i = 0; i < totalSlaves; i++) {
    slavePromises.push(
      invokeLambda(
        {
          slaveId: i,
          endpoint: testEndpoint,
          numberOfRequests: totalRequests / totalSlaves,
          requestsPerSecond: slaveRequestPerSecond
        },
        "Digital-mass-sns-test-module-dev-massSnsSlave"
      )
    );
  }
  const result = await Promise.all(slavePromises);
  console.log("Done.");
  console.log(result);
};

const invokeLambda = async (payload, functionName) => {
  console.log(`Invoking lambda function ${functionName}`);
  let params = {
    FunctionName: functionName,
    Payload: JSON.stringify(payload)
  };
  try {
    const response = await lambda.invoke(params).promise();
    return JSON.parse(response.Payload.toString());
  } catch (err) {
    throw err;
  }
};
